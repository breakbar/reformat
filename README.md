# Reformat

windows 10 Asus rog g501JW 467T

## Requis

- clé USB ou Disque (vierge et minimum 4go)
- 8go d'espace libre sur le disque C:
- [MediaCreationTool Windows 10 Software](https://www.microsoft.com/fr-fr/software-download/windows10) Pour télécharger windows 10
- wpkey Pour récupérer la clé windows actuelle
- [Informations pilotes du pc](https://www.asus.com/fr/support/Download/3/768/0/1/uv3gzPnU54cTZC6Y/45/)
- [nvidia](http://www.nvidia.fr/Download/index.aspx?lang=fr&selProductSeriesType=1&selProductSeries=99&selProductFamily=769&selOperatingSystem=57&ddlLanguage=12)

## Procédure

- Copier vos données importantes et personnelles sur un disque externe
- Libéré 8go + une marge sur le disque 
- Télécharger le software de windows 10
- Demander un support d'installation -> Selectionner votre support -> télécharger
- Installer Windows
- Après l'installation installer les pilotes (confère catégorie pilotes plus bas)

## Pilotes

Par ordre de priotité

1. Chipset/
    * Chipset_Intel_Skylake_Win10_64 - Intel INF Update Driver
2. Carte Graphique/
    * nvidia - nVidia Graphics Driver
3. Other/
    * MEI_Intel - Intel Management Engine Interface
4. Atk/
    * ATKPackage - ATKACPI driver and hotkey-related utilities
5. Connexion sans fil/
    * WiFi_intel - Intel WiFi Wireless LAN Driver
6. Carte Graphique/
    * VGA_Intel_Skylake - Intel Graphics Driver
7. Connexion sans fil/
    * WirelessRadioControl - ASUS Wireless Radio Control
8. Bluetooh/
    * Bluetoof_Intel - Intel BlueToof driver
9. Lecteur de carte/
    * CardReader_Realtek - Realtek Multi-Card Reader Driver
10. Pavé tactile/
    * SmartGesture - ASUS Smart Gesture (TouchPad Driver)
11. Autres/
    * IRST_Intel_Skylake - Intel Rapid Storage Technology driver
    * CPPC_Intel - Intel Collaborative Processor Performance Control Driver
    * PixelMasterVideoHDR - PixelMaster Video HDR
    * ThunderBolt_Intel - ThunderBolt
    * ROG_GamingMouse - Gaming Mouse Driver
12. Utilitaires/
    * GamingAssistant - Gaming Assistant (Si écran 4K)
    * ROG_Game_First - ROG Game First III
    * ASUS_LiveUpdate - ASUS Live Update
    * USB_Charger_Plus - ASUS USB Charger Plus
    * Splendid_Upgrade - ASUS Splendid
    * Wireless_Display_Intel - Intel Wireless Display Application
13. Audio/
    * Audio_Realtek - Realtek Audio Driver
    * [2xREBOOT]